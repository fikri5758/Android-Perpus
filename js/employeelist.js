var serviceURL = "http://unindraperpus.esy.es/services/";

var employees;

$('#employeeListPage').bind('pageinit', function(event) {
	getEmployeeList();
});

function getEmployeeList() {
	$.getJSON("http://unindraperpus.esy.es/services/" + 'getemployees.php', function(data) {
		$('#employeeList li').remove();
		employees = data.items;
		//console.log('================Debug saya===============');
		//console.log(data.items[0].biblio_id);
		$.each(employees, function(index, employee) {
			//console.log(employee.biblio_id);
			$('#employeeList').append('<li><a href="employeedetails.html?id=' + employee.biblio_id + '">' +
					'<img src="http://unindraperpus.esy.es/web/images/docs/' + employee.image + '"/>' +
					'<h4>' + employee.title + ' ' + employee.publish_year + '</h4>' +
					'<p>ISBN / ISSN : ' + employee.isbn_issn + '</p>');
					// '<span class="ui-li-count">' + employee.reportCount + '</span></a></li>');
		});
		$('#employeeList').listview('refresh');
	});
}