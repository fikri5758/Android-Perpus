$('#reportListPage').live('pageshow', function(event) {
	var id = getUrlVars()["id"];
	console.log("reports for " + id);
	$.getJSON("http://unindraperpus.esy.es/services/" + 'getreports.php?id='+id, function (data) {
		var reports = data.items;
		$.each(reports, function(index, employee) {
			$('#reportList').append('<li><a href="employeedetails.html?id=' + employee.id + '">' +
					'<h4>' + employee.title + ' ' + employee.publish_year + '</h4>' +
					'<p>' + employee.isbn_issn + '</p>' +
					'<span class="ui-li-count">' + employee.reportCount + '</span></a></li>');
		});
		$('#reportList').listview('refresh');
	});
});
