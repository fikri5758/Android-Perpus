$('#detailsPage').live('pageshow', function(event) {
	var id = getUrlVars()["id"];
	//console.log(id);
	$.getJSON('http://unindraperpus.esy.es/services/getemployee.php?id='+id, displayEmployee);
});

function displayEmployee(data) {
	var book = data.item;
	console.log(book);
	$('#bookPic').attr('src', 'http://unindraperpus.esy.es/web/images/docs/' + book.image);
	$('#bookTitle').text(book.title);
	$('#bookNotes').text(book.notes);
	$('#bookYear').text(book.publish_year);
	$('#fileBook').attr('src', 'http://unindraperpus.esy.es/web/images/docs/' + book.fileatt);
	//console.log(book.officePhone);
	/*if (book.managerId>0) {
		$('#actionList').append('<li><a href="employeedetails.html?id=' + book.managerId + '"><h3>View Manager</h3>' +
				'<p>' + book.managerFirstName + ' ' + book.managerLastName + '</p></a></li>');
	}
	if (book.reportCount>0) {
		$('#actionList').append('<li><a href="reportlist.html?id=' + book.id + '"><h3>View Direct Reports</h3>' +
				'<p>' + book.reportCount + '</p></a></li>');
	}
	if (book.email) {
		$('#actionList').append('<li><a href="mailto:' + book.email + '"><h3>Email</h3>' +
				'<p>' + book.email + '</p></a></li>');
	}
	if (book.officePhone) {
		$('#actionList').append('<li><a href="tel:' + book.officePhone + '"><h3>Call Office</h3>' +
				'<p>' + book.officePhone + '</p></a></li>');
	}
	if (book.cellPhone) {
		$('#actionList').append('<li><a href="tel:' + book.cellPhone + '"><h3>Call Cell</h3>' +
				'<p>' + book.cellPhone + '</p></a></li>');
		$('#actionList').append('<li><a href="sms:' + book.cellPhone + '"><h3>SMS</h3>' +
				'<p>' + book.cellPhone + '</p></a></li>');
	}*/
	$('#actionList').listview('refresh');
	
}

/*function getUrlVars() {
    var vars = [], hash;
    console.log(window.location.href);
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}*/

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');

    	//console.log(hash);
        if($.inArray(hash[0], vars)>-1)
        {
            vars[hash[0]]+=","+hash[1];
        }
        else
        {
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
    }
    //console.log(vars['id']);
    return vars;
}
